import React, { Component } from 'react'
import MovieService from '../MovieService'

class ListMovieComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            movies: []
        }
        this.addMovie = this.addMovie.bind(this);
        this.editMovie = this.editMovie.bind(this);
        this.deleteMovie = this.deleteMovie.bind(this);
    }


    deleteMovie(id){
        MovieService.deleteMovie(id).then( res => {
            this.setState({movies: this.state.movies.filter(movie => movie.id !== id)});
        });
    }
    viewMovie(id){
        this.props.history.push(`/view-movie/${id}`);
    }


    editMovie(id){
        this.props.history.push(`/add-movie/${id}`);
    }

    componentDidMount(){
        MovieService.getMovies().then((res) => {
            this.setState({ movies: res.data});
        });
    }

    addMovie(){
        this.props.history.push('/add-movie/_add');
    }

    render() {
        return (
            <div>
                 <h2 className="text-center">Movies List</h2>
                 <div className = "row">
                    <button className="btn btn-primary" onClick={this.addMovie}> Add Movie</button>
                 </div>
                 <br></br>
                 <div className = "row">
                        <table className = "table table-striped table-bordered">

                            <thead>
                                <tr>
                                    <th> Movie Number</th>
                                    <th> Movie Name</th>
                                    <th> Movie Imbd</th>
                                    <th> Movie Category</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.movies.map(
                                        movie => 
                                        <tr key = {movie.id}>
                                             <td> {movie.movieNumber}</td>   
                                             <td> {movie.movieName} </td>   
                                             <td> {movie.imbd}</td>
                                             <td> {movie.movieCategory}</td>
                                             <td>
                                                 <button onClick={ () => this.editMovie(movie.id)} className="btn btn-info">Update </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.deleteMovie(movie.id)} className="btn btn-danger">Delete </button>
                                                 <button style={{marginLeft: "10px"}} onClick={ () => this.viewMovie(movie.id)} className="btn btn-info">View </button>
                                             </td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>

                 </div>

            </div>
        )
    }
}

export default ListMovieComponent

