import React, { Component } from 'react'
import MovieService from './MovieService'

class CreateMovieComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            // step 2
            id: this.props.match.params.id,
            movieNumber: '',
            movieName: '',
            imbd: '',
            movieCategory: ''
        }
        this.changeMovieNumberHandler = this.changeMovieNumberHandler.bind(this);
        this.changeMovieNameHandler = this.changeMovieNameHandler.bind(this);
        this.changeImbdHandler = this.changeImbdHandler.bind(this);
        this.changeMovieCategoryHandler = this.changeMovieCategoryHandler.bind(this);
        this.saveOrUpdateMovie = this.saveOrUpdateMovie.bind(this);
    }

    // step 3
    componentDidMount(){

        // step 4
        if(this.state.id ==='_add'){
            return
        }else{
            MovieService.getMovieById(this.state.id).then( (res) =>{
                let movie = res.data;
                this.setState({movieNumber: movie.movieNumber,
                    movieName: movie.movieName,
                    imbd : movie.imbd,
                    movieCategory:movie.movieCategory
                });
            });
        }        
    }
/*
saveMovie = (e) => {
    e.preventDefault();
    let movie = {movieNumber: this.state.movieNumber, movieName: this.state.movieName, imbd: this.state.imbd,movieCategory : this.state.movieCategory};
        console.log('movie => ' + JSON.stringify(movie))
}
*/
    saveOrUpdateMovie = (e) => {
        e.preventDefault();
                 
        let movie = {movieNumber: this.state.movieNumber, movieName: this.state.movieName, imbd: this.state.imbd,movieCategory : this.state.movieCategory};
        console.log('movie => ' + JSON.stringify(movie));

        // step 5
        if(this.state.id ==='_add'){
            MovieService.createMovie(movie).then(res =>{
                this.props.history.push('/movies');
            });
        }else{
            MovieService.updateMovie(movie, this.state.id).then( res => {
                this.props.history.push('/movies');
            });
        }
    }
    
    changeMovieNumberHandler= (event) => {
        this.setState({movieNumber: event.target.value});
    }

    changeMovieNameHandler= (event) => {
        this.setState({movieName: event.target.value});
    }

    changeImbdHandler= (event) => {
        this.setState({imbd: event.target.value});
    }

    changeMovieCategoryHandler= (event) => {
        this.setState({movieCategory: event.target.value});
    }

    cancel(){
        this.props.history.push('/movies');
    }

    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Add Movie</h3>
        }else{
            return <h3 className="text-center">Update Movie</h3>
        }
    }
    render() {
        return (
            <div>
                <br></br>
                   <div className = "container">
                        <div className = "row">
                            <div className = "card col-md-6 offset-md-3 offset-md-3">
                                {
                                    this.getTitle()
                                }
                                <div className = "card-body">
                                    <form>
                                    <div className = "form-group">
                                            <label> Movie Number: </label>
                                            <input placeholder="Movie Number" name="movieNumber" className="form-control" 
                                                value={this.state.movieNumber} onChange={this.changeMovieNumberHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Movie Name: </label>
                                            <input placeholder="Movie Name" name="movieName" className="form-control" 
                                                value={this.state.movieName} onChange={this.changeMovieNameHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Imbd : </label>
                                            <input placeholder="Imbd " name="imbd " className="form-control" 
                                                value={this.state.imbd} onChange={this.changeImbdHandler}/>
                                        </div>
                                        <div className = "form-group">
                                            <label> Movie Category : </label>
                                            <input placeholder="Movie Category " name="movieCategory " className="form-control" 
                                                value={this.state.movieCategory} onChange={this.changeMovieCategoryHandler}/>
                                        </div>

                                        <button className="btn btn-success" onClick={this.saveOrUpdateMovie}>Save</button>
                                        <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft: "10px"}}>Cancel</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                   </div>
            </div>
        )
    }
}

export default CreateMovieComponent

