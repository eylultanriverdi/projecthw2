import axios from 'axios';

const MOVIE_API_BASE_URL = "http://localhost:8080/api/v1/movies";

class MovieService {

    async getMovies(){
       const responst = await axios.get(MOVIE_API_BASE_URL,{
       headers : {"Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "POST",
        "Access-Control-Allow-Headers":"*"}
       });
       console.log(responst)
    }

    createMovie(movie){
        return axios.post(MOVIE_API_BASE_URL, movie);
    
    }

    getMovieById(movieId){
        return axios.get(MOVIE_API_BASE_URL + '/' + movieId);
    }

    updateMovie(movie, movieId){
        return axios.put(MOVIE_API_BASE_URL + '/' + movieId, movie);
    }

    deleteMovie(movieId){
        return axios.delete(MOVIE_API_BASE_URL + '/' + movieId);
    }
}

export default new MovieService()







