import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import ListMovieComponent from './components/ListMovieComponent';
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import CreateMovieComponent from './CreateMovieComponent';
import ViewMovieComponent from './ViewMovieComponent';

function App() {
  return (
    <div>
        <Router>
              <HeaderComponent />
                <div className="container">
                    <Switch> 
                          <Route path = "/" exact component = {ListMovieComponent}></Route>
                          <Route path = "/movies" component = {ListMovieComponent}></Route>
                          <Route path = "/add-movie/:id" component = {CreateMovieComponent}></Route>
                          <Route path = "/view-movie/:id" component = {ViewMovieComponent}></Route>
                          {/* <Route path = "/update-movie/:id" component = {UpdateMovieomponent}></Route> */}
                    </Switch>
                </div>
              <FooterComponent />
        </Router>
    </div>
    
  );
}

export default App;

