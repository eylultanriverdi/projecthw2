import React, { Component } from 'react'
import MovieService from './MovieService'

class ViewMovieComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            movie: {}
        }
    }

    componentDidMount(){
        MovieService.getMovieById(this.state.id).then( res => {
            this.setState({movie: res.data});
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <div className = "card col-md-6 offset-md-3">
                    <h3 className = "text-center"> View MOvie Details</h3>
                    <div className = "card-body">
                        <div className = "row">
                            <label> Movie Number: </label>
                            <div> { this.state.movie.movieNumber }</div>
                        </div>
                        <div className = "row">
                            <label> MOvie Name: </label>
                            <div> { this.state.movie.movieName }</div>
                        </div>
                        <div className = "row">
                            <label> Imbd: </label>
                            <div> { this.state.movie.imbd }</div>
                        </div>
                        <div className = "row">
                            <label> Movie Category: </label>
                            <div> { this.state.movie.movieCategory }</div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default ViewMovieComponent

